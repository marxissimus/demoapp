<?php  

require '../src/models/tag.php';
require '../src/models/post_tag.php';

class Post extends \Illuminate\Database\Eloquent\Model {  
  protected $table = 'posts';

  public function category(){
    return $this->belongsTo('Category');
  }

  public function post_tags() {
    return $this->hasMany('PostTag');
  }

  public function tags() {
    return $this->hasManyThrough('Tag', 'PostTag');
  }

}
