<?php

require '../vendor/autoload.php';
require '../src/models/item.php';
require '../src/models/job.php';
require '../src/models/stock.php';
require '../src/models/category.php';
require '../src/models/post.php';

$config = include('../src/config.php');

$app = new \Slim\App(['settings'=> $config]);

$container = $app->getContainer();

$capsule = new \Illuminate\Database\Capsule\Manager;
$capsule->addConnection($container['settings']['db']);
$capsule->setAsGlobal();
$capsule->bootEloquent();


$app->get('/posts', function($request, $response){


  /*
  * Post type - headline, top, recent, trending, defaults to recent
  *
  * recent   => ORDER BY posts.date DESC
  * headline => in_array('headline', $post->tags)
  * top      => having top likes/comments
  * trending => ?? 
  *
  */

  $type = $request->getQueryParam('type', $default='recent');

  /*
  * Main query => fetches all posts with related category information
  */  
  $posts_with_categories = "SELECT posts.id, posts.title, posts.guid, posts.date, posts.content, posts.thumbnail_url, posts.comments, categories.id AS cat_id, categories.name AS cat_name FROM posts JOIN categories  ON categories.id = posts.category_id";

  /*
  * Pagination details
  * ___________________ 
  *
  * Request params
  *
  *  $page => defaults to 1
  *  $num_of_posts => how many posts should be shown, defaults to 10
  * 
  * Mediums
  *
  *  $offset => simple OFFSET
  * 
  * Count
  *
  *  $posts_count => shows count of all posts
  *
  */

  $page = intval($request->getQueryParam('page', $default=1));
  $num_of_posts = intval($request->getQueryParam('num_of_posts', $default=10));
  $offset = ($page-1)*$num_of_posts; 
  
  $posts_count = Post::count();

  switch ($type) {
    case 'headline':
      $query = $posts_with_categories." WHERE posts.id in (SELECT post_id FROM post_tags where tag_id in (SELECT id FROM tags WHERE name = 'Headline'))";    
      break;
    case 'top':
      $query = $posts_with_categories. " ORDER BY posts.comments DESC LIMIT {$num_of_posts}";
      break;
    case 'trending':
      $query = $posts_with_categories;
      break;
    default:
      $query = $posts_with_categories." ORDER BY STR_TO_DATE(posts.date, '%M %d, %Y') DESC LIMIT {$num_of_posts} OFFSET {$offset}";
      break;
  }


  $res = \Illuminate\Database\Capsule\Manager::select($query);
  $res = (array) $res;
  foreach ($res as &$r) {
    $pt = PostTag::where('post_id', $r->id)->pluck('tag_id')->toArray();
    $tags = Tag::whereIn('id', $pt)->get()->toArray();
    $r->tags = $tags;
  }

  $ret = ['current_posts_count' => $num_of_posts, 'total_posts_count' => $posts_count, 'next_page' => $page+1];
  $ret['data'] = $res;

  $body = $response->getBody();
  $body->rewind();
  $body->write(json_encode($ret));

  return $response
      ->withHeader('Content-Type', 'application/json;charset=utf-8')
      ->withHeader('Access-Control-Allow-Origin', '*')
      ->withHeader('Access-Control-Allow-Methods', 'GET');
});

$app->get('/items', function($request, $response, $args) {


  $page = intval($request->getQueryParam('page', $default=1));

  $num_of_items = intval($request->getQueryParam('num_of_items', $default=10));

  $offset = ($page-1)*$num_of_items; 

  $items = Item::skip($offset)->take($num_of_items)->get();
  $total = Item::count();

  $ret = ['current_items_count' => $num_of_items, 'total_items_count' => $total, 'next_page' => $page+1];

  $ret['data'] = $items;

  $body = $response->getBody();
  $body->rewind();
  $body->write(json_encode($ret));

  return $response
      ->withHeader('Content-Type', 'application/json;charset=utf-8')
      ->withHeader('Access-Control-Allow-Origin', '*')
      ->withHeader('Access-Control-Allow-Methods', 'GET');
});

$app->get('/jobs', function($request, $response) {

  $page = intval($request->getQueryParam('page', $default=1));

  $num_of_items = intval($request->getQueryParam('num_of_jobs', $default=10));

  $offset = ($page-1)*$num_of_items; 

  $items = Job::skip($offset)->take($num_of_items)->get();
  $total = Job::count();

  $ret = ['current_items_count' => $num_of_items, 'total_items_count' => $total, 'next_page' => $page+1];

  $ret['data'] = $items;

  $body = $response->getBody();
  $body->rewind();
  $body->write(json_encode($ret));

  return $response
      ->withHeader('Content-Type', 'application/json;charset=utf-8')
      ->withHeader('Access-Control-Allow-Origin', '*')
      ->withHeader('Access-Control-Allow-Methods', 'GET');

});


$app->get('/stocks', function($request, $response) {

  $stocks = Stock::latest()->first();

  $ret = ['count' => count($stocks), 'data' => $stocks];
  
  $body = $response->getBody();
  $body->rewind();
  $body->write(json_encode($ret));

  return $response
      ->withHeader('Content-Type', 'application/json;charset=utf-8')
      ->withHeader('Access-Control-Allow-Origin', '*')
      ->withHeader('Access-Control-Allow-Methods', 'GET');
});

$app->get('/stocks/all', function($request, $response) {

  $stocks = Stock::all()->toJson();

  $body = $response->getBody();
  $body->rewind();
  $body->write($stocks);

  return $response
      ->withHeader('Content-Type', 'application/json;charset=utf-8')
      ->withHeader('Access-Control-Allow-Origin', '*')
      ->withHeader('Access-Control-Allow-Methods', 'GET');

});


$app->run();
